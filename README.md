ORC: Coding Exercise for Developers
===

Question
--------
The question is to design and implement a simple **echo server** and **echo client** with measurement on latency
and throughput. The programming is designated to C++.

Introduction
------------
Several high-level considerations on accomplishing the project are listed
```
* Construct from scratch without using C++ third-party library
* Encapsulate POSIX socket programming interface using Object Oriented (OO) techniques
* Hierarchies of types (classes) reflects inner relationship between them
* Final API should be very elegant and easy to use
* Follow RAII (Resource Acquisition Is Initialization) design principle as possible
* The developing of the project is a Test-driven development (TDD)
```
> Note: Go through to the 'Build and Run' section for quick testing and fun!

Design
------

Class hierarchy can be easily seen from headers of each module (type).

> Note: Several C++11 features are used, e.g. `constexpr`.

Project Structure
-----------------
The project is named `orc` and coded in the **namespace** `orc` in sources.
Before building the file hierarchies are as follows.
```
orc
├── README.md
├── build
├── clean
├── data
│   └── apple.txt
├── old
│   ├── client_echo.h
│   └── server_echo.h
├── src
│   ├── Makefile
│   ├── applet.cc
│   ├── applet.h
│   ├── applet_test.cc
│   ├── client.cc
│   ├── client.h
│   ├── client_test.cc
│   ├── echo_client.cc
│   ├── echo_server.cc
│   ├── logger.cc
│   ├── logger.h
│   ├── logger_test.cc
│   ├── message.cc
│   ├── message.h
│   ├── message_test.cc
│   ├── orc.h
│   ├── server.cc
│   ├── server.h
│   ├── server_test.cc
│   ├── socker.cc
│   ├── socker.h
│   ├── socker_test.cc
│   ├── timing.cc
│   ├── timing.h
│   └── timing_test.cc
└── stat
    └── apple.stat
```



Build and Run
-------------
It is straightforward to build the package by just running the **bash** script `build`. The output is as follows.
```bash
[150330:075737][/Users/LIYING/ORC/orc]build
clang++ -std=c++11 -stdlib=libc++ -c -w timing.cc -o timing.o
clang++ -std=c++11 -stdlib=libc++ -c -w logger.cc -o logger.o
clang++ -std=c++11 -stdlib=libc++ -c -w applet.cc -o applet.o
clang++ -std=c++11 -stdlib=libc++ -c -w message.cc -o message.o
clang++ -std=c++11 -stdlib=libc++ -c -w socker.cc -o socker.o
clang++ -std=c++11 -stdlib=libc++ -c -w server.cc -o server.o
clang++ -std=c++11 -stdlib=libc++ -c -w client.cc -o client.o
ar cru liborc.a timing.o logger.o applet.o message.o socker.o server.o client.o
clang++ -std=c++11 -stdlib=libc++ -w echo_server.cc -o echoserver  -I./ -L./ -lorc
clang++ -std=c++11 -stdlib=libc++ -w echo_client.cc -o echoclient  -I./ -L./ -lorc
```
After running `build` successfully, the double executables `echoserver` and `echoclient` appeared under the
home directory of the project - `orc`.
```bash
[150330:080242][/Users/LIYING/ORC/orc]l
total 168
-rw-r--r--   1 LIYING  staff   1177 Mar 30 08:02 README.md
-rwxr-xr-x   1 LIYING  staff     90 Mar 30 07:36 build
-rwxr-xr-x   1 LIYING  staff     68 Mar 30 07:37 clean
drwxr-xr-x   3 LIYING  staff    102 Mar 30 06:59 data
-rwxr-xr-x   1 LIYING  staff  35668 Mar 30 08:02 echoclient
-rwxr-xr-x   1 LIYING  staff  34808 Mar 30 08:02 echoserver
drwxr-xr-x   4 LIYING  staff    136 Mar 30 05:24 old
drwxr-xr-x  37 LIYING  staff   1258 Mar 30 08:02 src
drwxr-xr-x   3 LIYING  staff    102 Mar 30 07:32 stat
```

Usages of them are as follows.
```
usage: echoserver [-D] [name] [host] [port]
   -D: run as a daemon (Not supported yet!)
 name: applet name
 host: server host name in IP
 port: server port number
email: liyinglance@gmail.com
```

```
usage: echoclient [-D] [name] [host] [port]
   -D: run as a daemon (Not supported yet!)
 name: applet name
 host: server host name in IP
 port: server port number
email: liyinglance@gmail.com
```
Simplest run of the server and client could be as follows.
```
[150330:081425][/Users/LIYING/ORC/orc]echoserver
20150330081429: server(trace): start running!
20150330081429: server(trace): start listening on 127.0.0.1.8848!
...
```

```
[150330:082028][/Users/LIYING/ORC/orc]echoclient
20150330082030: server(trace): start running!
...
```

Benchmarking
------------
Benchmarking data is the adjusted daily stock prices of **Apple, Inc.** downloaded from Yahoo! finance.
The format of each message are `type` and `size` pattern.
However, for this particular benchmarking data, `type` is filled by `date` integer and `size` is filled by `price` integer transformed from double floating money number to integer.
This transformation simplified the procedure of benchmarking **statistical** data generation.

Statistical data on a MacBook Air computer was generated and located in the `stat` directory by the following command.

```
echoclient < ./data/apple.txt > ./stat/apple.stat
```
