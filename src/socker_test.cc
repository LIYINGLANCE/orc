#include "socker.h"
#include <iostream>
#include <stdio.h>

using namespace orc;

int main()
try {
	Socker sock("127.0.0.1", 8848);
	fprintf(stderr, "Communication Endpoint: [%s.%d]\n", sock.gethost(), sock.getport());
	Client conn("127.0.0.1", 8849);
}
catch (const std::exception& e) {
	std::cerr << "Exception Caught: " << e.what() << std::endl;
}
