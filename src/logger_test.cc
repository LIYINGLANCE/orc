#include "logger.h"
#include <iostream>

using namespace orc;

int main()
try {
	Logger lg("logger-test", Severity::stats);
	lg.setindent("LOG");
	Logger screen(1, Severity::trace);
	screen.setindent("screen: >> ");
	screen << "Logger is saying Hello!!!";
	lg << "Hello, logger output through overloaded operator << !!";
	lg.trace("%d: %s", 200, "trace is logging something!");
	lg.error("%d: %s", 200, "error is logging something!");
	lg.stats("%d: %s", 200, "stats is logging something!");
	lg << NULL;
}
catch(const std::exception& e){
	std::cerr << "Exception Caught: " << e.what() << std::endl;
}
