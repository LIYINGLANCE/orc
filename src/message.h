#ifndef MESSAGE_H
#define MESSAGE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#define numdigitschar 5
#define numdigitslong 5
#define numdigitsmssg 11

#define messageformat "%05ld%05ld"

namespace orc {

struct message {
	long  type;
	long  size;

	message(){}
	message(long t, long s);
	message& operator=(const message&);

	friend char *encode(message *m);
	friend message decode(char *ms);
	friend int messagecmp(message s, message t);
};

char *encode(message *m);
message decode(char *ms);
int messagecmp(message s, message t);

int putmessage(int des, message *m);
int getmessage(int des, message *m);

} //namespace orc
#endif
