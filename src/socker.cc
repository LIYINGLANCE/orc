#include "socker.h"

namespace orc {

Socker::Socker(const char *host, short port) : bound_(false), connected_(false)
{
	address_.sin_family = AF_INET;
	address_.sin_addr.s_addr = inet_addr(host);
	address_.sin_port = htons(port);

	addresslength_ = (socklen_t)sizeof(address_);

	sock_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock_ < 0) throw std::runtime_error("Socker: socket failed!");
}

Socker::~Socker()
{
	if (connected_ && (::close(sock_) < 0)) throw std::runtime_error("Socker: close failed!");
	connected_ = false;
}

Socker::Socker(const Socker& sock)
{
	bound_ = sock.bound_;
	connected_ = sock.connected_;
	sock_ = sock.sock_;
	address_ = sock.address_;
	addresslength_ = sock.addresslength_;
}

Socker& Socker::operator=(const Socker& sock)
{
	bound_ = sock.bound_;
	connected_ = sock.connected_;
	sock_ = sock.sock_;
	address_ = sock.address_;
	addresslength_ = sock.addresslength_;
	return *this;
}

Socker::Socker(int sock, struct sockaddr_in address, socklen_t addresslength)
: bound_(false), connected_(true)
{
	sock_ = sock;
	address_ = address;
	addresslength_ = addresslength;
}

char *Socker::gethost()
{
	return inet_ntoa(address_.sin_addr);
}

short Socker::getport()
{
	return ntohs(address_.sin_port);
}

void Socker::setlinger(int timeout)
{
	struct linger solinger;
	solinger = {1, timeout};
	int result = setsockopt(sock_, SOL_SOCKET, SO_LINGER, &solinger, sizeof(solinger));
	if (result < 0) throw std::runtime_error("Socker: setlinger failed!");
}

void Socker::close()
{
	if (::close(sock_) < 0) throw std::runtime_error("Socker: close failed!");
	connected_ = false;
}


long Socker::get(char *store, int size)
{
	if (store == NULL) return -1;

	ssize_t all = 0;
	ssize_t num = 0;

	while (all < size) {
		while (((num = recv(sock_, store + all, size - all, 0)) == -1) && (errno == EINTR));
		//!!NOTE:For TCP sockets, the return value 0 means the peer has closed its half side of the connection.
		if (num == 0) break;
		all += num;
	}

	return all;
}

long Socker::put(char *store, int size)
{
	if (store == NULL) return -1;

	ssize_t all = 0;
	ssize_t num = 0;

	while (all < size) {
		while (((num = send(sock_, store + all, size - all, 0)) == -1) && (errno == EINTR));
		all += num;
	}

	return all;
}

long Socker::getmessage(message *m)
{
	char buffer[numdigitsmssg];
	int num = get(buffer, numdigitsmssg);
	*m = decode(buffer);
	return num;
}

long Socker::putmessage(message *m)
{
	return put(encode(m), numdigitsmssg);
}

} //namespace orc
