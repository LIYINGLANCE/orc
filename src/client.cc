#include "client.h"

namespace orc {

Client::Client(const char *name, const char *host, short port)
: Applet(name), Socker(host, port)
{}

Client::Client(const char *name, const char *logname, const char *host, short port)
: Applet(name, logname), Socker(host, port)
{}

Client::Client(const char *name, int logdes, const char *host, short port)
: Applet(name, logdes), Socker(host, port)
{}

void Client::connect()
{
	int result = ::connect(sock_, (struct sockaddr *)(&address_), (socklen_t)(sizeof(address_)));
	if (result < 0) throw std::runtime_error("Client connect failed!");
	connected();
}

} //namespace orc
