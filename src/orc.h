#ifndef ORC_H
#define ORC_H

#include "message.h"
#include "timing.h"
#include "logger.h"
#include "applet.h"
#include "socker.h"
#include "server.h"
#include "client.h"

#include <iostream>

constexpr int maxargsize = 16;

#endif
