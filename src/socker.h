#ifndef SOCKER_H
#define SOCKER_H

#include "message.h"

#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <exception>
#include <stdexcept>
#include <system_error>

namespace orc {

class Socker {
public:

	Socker(const char *host, short port);
	~Socker();

	Socker(){}
	Socker(const Socker&);
	Socker& operator=(const Socker&);

	Socker(int sock, struct sockaddr_in address, socklen_t addresslength);

	char *gethost();
	short getport();

	long get(char *store, int size);
	long put(char *store, int size);

	long putmessage(message *m);
	long getmessage(message *m);

	void close();
	inline void connected(){ connected_ = true; }
	void setlinger(int timeout);

protected:
	bool bound_;
	bool connected_;
	int sock_;
	struct sockaddr_in address_;
	socklen_t addresslength_;
};

typedef Socker Acceptor;
typedef Socker Connector;

} //namespace orc
#endif
