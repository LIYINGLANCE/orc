#ifndef TIMING_H
#define TIMING_H

#include <unistd.h>
#include <time.h>
#include <sys/time.h>

namespace orc {

constexpr double million = 1000000.0;
constexpr long M = 1000000;
constexpr long K = 1000;


class timepoint {
public:
	timepoint(){ gettimeofday(&t_, NULL); }

	double get();
	long getus();

	friend class timerange;
private:
	struct timeval t_;
};

class timerange {
public:
	timerange(const timepoint& t1, const timepoint& t2)
 	{
 		t1_ = t1.t_;
 		t2_ = t2.t_;
 	}

	double get();
	long getus();
private:
	struct timeval t1_;
	struct timeval t2_;
};

int microsleep(long microseconds);
char *timestamp();

} //namespace orc

#endif
