#include "orc.h"

using namespace orc;

int usage()
{
    std::cerr << "usage: echoclient [-D] [name] [host] [port]" << std::endl;
    std::cerr << "   -D: run as a daemon (Not supported yet!)" << std::endl;
    std::cerr << " name: applet name" << std::endl;
    std::cerr << " host: server host name in IP" << std::endl;
    std::cerr << " port: server port number" << std::endl;
    std::cerr << "email: liyinglance@gmail.com" << std::endl;
    return 0;
}

message parsemessage(char *s)
{
	message m;
    char *input = strdup(s);
    char *token = NULL;
    for (int t = 0; (token = strsep(&input, "\t")) != NULL && t < 2; t++)
    {
        switch(t)
        {
            case 0: m.type = atol(token); break;
            case 1: m.size = atol(token); break;
            default: break;
        }
    }
    return m;
}

int main(int argc, char *argv[])
try {

    usage();

    char name[maxargsize] = "server";
    char host[maxargsize] = "127.0.0.1";
    char port[maxargsize] = "8848";

    int console = 1;
    if (getopt(argc, argv, "D") != 'D') console = true;
    argc -= optind;
    argv += optind;

    if (argc > 0) { snprintf(name, maxargsize, "%s", *argv); argc--; argv++; }
    if (argc > 0) { snprintf(host, maxargsize, "%s", *argv); argc--; argv++; }
    if (argc > 0) { snprintf(port, maxargsize, "%s", *argv); argc--; argv++; }
    short portno= (short) atoi(port);


	Client c(name, 1, host, portno);
	c.trace("start running!");

	c.connect();
	c.trace("connected!!!");

    // c.setlevel(Severity::stats);

	char *line = NULL;
    size_t capacity = 0;
    ssize_t linelen;

    double rtt = 0.0;
    long counter = 0;
    for (counter = 0; (linelen = getline(&line, &capacity, stdin)) > 0; counter++) {
		message m = parsemessage(line);
		message n;

        timepoint p;
        c.putmessage(&m);
		c.getmessage(&n);
        timepoint q;

        timerange r(p,q);
        c.stats("round trip latency for message %04ld: %ld microseconds!", counter, r.getus());
        rtt += r.get();
        c.trace("message put & got: %s", encode(&n));
    }

    c.stats("runnning average latency: %.9f seconds/message!", rtt/counter);
    c.stats("runnning average throughput: %.9f bytes/second!", counter*numdigitsmssg/rtt);
}
catch(const std::exception& e){
	std::cerr << "Exception Caught: " << e.what() << std::endl;
}
