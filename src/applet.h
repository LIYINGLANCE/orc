#ifndef APPLET_H
#define APPLET_H

#include "logger.h"

#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <signal.h>
#include <syslog.h>

#include <exception>
#include <stdexcept>
#include <system_error>

#define NOCHDIR NULL
#define CLOSEFD 0

namespace orc {

constexpr int namesize = 20;

class Applet : public Logger {
public:
	Applet(const char *name);
	Applet(const char *name, const char *logname);
	Applet(const char *name, int logdes);
	~Applet();

	int daemon(char *dir = NOCHDIR, int noclose = !CLOSEFD);
private:
	char *name_;
};

} //namespace orc

#endif
