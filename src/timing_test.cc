#include "timing.h"
#include <iostream>

using namespace orc;

int main()
try {
	fprintf(stderr, "%s: enter!\n", timestamp());

	timepoint start;

	sleep(3);

	timepoint currt;
	timerange range(start, currt);

	fprintf(stderr, "since: %.9f time elapse: %.9f seconds!\n", start.get(), range.get());
	fprintf(stderr, "since: %ld time elapse: %ld microseconds!\n", start.getus(), range.getus());

	fprintf(stderr, "%s: leave!\n", timestamp());
}
catch(const std::exception& e){
	std::cerr << "Exception Caught: " << e.what() << std::endl;
}
