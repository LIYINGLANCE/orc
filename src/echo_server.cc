#include "orc.h"

using namespace orc;

int usage()
{
	std::cerr << "usage: echoserver [-D] [name] [host] [port]" << std::endl;
	std::cerr << "   -D: run as a daemon (Not supported yet!)" << std::endl;
    std::cerr << " name: applet name" << std::endl;
    std::cerr << " host: server host name in IP" << std::endl;
    std::cerr << " port: server port number" << std::endl;
	std::cerr << "email: liyinglance@gmail.com" << std::endl;
	return 0;
}

int main(int argc, char *argv[])
try {

	usage();

	char name[maxargsize] = "server";
	char host[maxargsize] = "127.0.0.1";
	char port[maxargsize] = "8848";

	int console = 1;
	if (getopt(argc, argv, "D") != 'D') console = true;
	argc -= optind;
	argv += optind;

	if (argc > 0) { snprintf(name, maxargsize, "%s", *argv); argc--; argv++; }
	if (argc > 0) { snprintf(host, maxargsize, "%s", *argv); argc--; argv++; }
	if (argc > 0) { snprintf(port, maxargsize, "%s", *argv); argc--; argv++; }
	short portno= (short) atoi(port);


	Server s(name, 1, host, portno);
	s.trace("start running!");

	s.listen();
	s.trace("start listening on %s.%d!", s.gethost(), s.getport());

	Acceptor a = s.accept();
	s.trace("accepted!!!");
	s.close();
	s.trace("listening socket closed!");

	int counter, infinite = 1;
	for (counter = 1; infinite; counter++) {
		message m;
		if (a.getmessage(&m) == 0) break;
		a.putmessage(&m);
		s.trace("message got & put: %s", encode(&m));
	}

}
catch(const std::exception& e){
	std::cerr << "Exception Caught: " << e.what() << std::endl;
}
