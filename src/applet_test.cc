#include "applet.h"
#include <iostream>

using namespace orc;

int main()
try {
	Applet app_1("app_1");
	app_1.trace("application no. %d", 1);

	Applet app_2("app_2", "app-2");
	app_2.daemon();
	app_2.stats("application no. %d", 2);

	Applet app_3("app_3", 1);
	app_3.error("application no. %d", 3);
}
catch(const std::exception& e){
	std::cerr << "Exception Caught: " << e.what() << std::endl;
}
