#include "timing.h"

namespace orc {


double timepoint::get()
{
	return t_.tv_sec + t_.tv_usec / million;
}

long timepoint::getus()
{
	return t_.tv_sec * M + t_.tv_usec;
}

double timerange::get()
{
	return ( t2_.tv_sec - t1_.tv_sec ) + ( t2_.tv_usec - t1_.tv_usec ) / million;
}

long timerange::getus()
{
	return ( t2_.tv_sec - t1_.tv_sec ) * M + ( t2_.tv_usec - t1_.tv_usec );
}

int microsleep(long microseconds)
{
	struct timespec ts;
	ts.tv_sec = microseconds / M;
	ts.tv_nsec = (microseconds % M) * K;
	return nanosleep(&ts, NULL);
}

char* timestamp()
{
	static char ts[0x5f];
	time_t t; time(&t);
	strftime(ts, 0x5f, "%Y%m%d%H%M%S", localtime(&t));
	return ts;
}

} //namespace orc
