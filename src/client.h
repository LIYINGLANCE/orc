#ifndef CLIENT_H
#define CLIENT_H

#include "socker.h"
#include "applet.h"

namespace orc {

class Client : public Applet, public Socker {
public:

	Client(const char *name, const char *host, short port);
	Client(const char *name, const char *logname, const char *host, short port);
	Client(const char *name, int logdes, const char *host, short port);
	~Client(){}

	void connect();
};

} //namespace orc
#endif
