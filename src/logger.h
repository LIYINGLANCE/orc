#ifndef LOGGER_H
#define LOGGER_H

#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <exception>
#include <stdexcept>
#include <system_error>

namespace orc {

constexpr int indentsize = 20;

enum class Severity { error = 3, stats = 6, trace = 7 };

class Logger {
public:
	Logger(const char *name, Severity level);
	Logger(int descriptor, Severity level);
	~Logger();

	inline void trace(const char *formant, ...);
	inline void error(const char *formant, ...);
	inline void stats(const char *formant, ...);
	inline void operator<< (const char *message);

	void setindent(const char *indent);
	void setlevel(Severity level);

private:
	ssize_t vprintf(Severity priority, const char *format, va_list arglist);
	static ssize_t dogwrite(int filedes, void *buffer, size_t size);
	char *timestamp();
	char *getseverity(Severity priority);

	Severity level_;
	int descriptor_;
	char *indent_;
};

inline void Logger::operator<< (const char *message)
{
	if (message == NULL) throw std::invalid_argument("Logger::operator<<!");
	trace("%s", message);
}

inline void Logger::trace(const char *format, ...)
{
	va_list arglist; va_start(arglist, format);
	ssize_t n = vprintf(Severity::trace, format, arglist);
	if (n < 0) throw std::runtime_error("Logger::trace!");
}

inline void Logger::error(const char *format, ...)
{
	va_list arglist; va_start(arglist, format);
	ssize_t n = vprintf(Severity::error, format, arglist);
	if (n < 0) throw std::runtime_error("Logger::error!");
}

inline void Logger::stats(const char *format, ...)
{
	va_list arglist; va_start(arglist, format);
	ssize_t n = vprintf(Severity::stats, format, arglist);
	if (n < 0) throw std::runtime_error("Logger::stats!");
}

} //namespace orc

#endif
