#include "server.h"
#include "timing.h"
#include <iostream>

using namespace orc;

int main()
try {

	Server s("server", 1, "127.0.0.1", 8848);
	s.trace("start running!");
	s.listen();
	s.trace("start listening on %s.%d!", s.gethost(), s.getport());
	s.setlinger(30);
	s.trace("set linger 30 seconds!");

	Acceptor a = s.accept();
	s.trace("accepted!!!");
	s.close();
	s.trace("listening socket closed!");

	char store[15];
	a.get(store, 13);
	s.trace("data received: %s", store);
	a.put(store, 14);
	s.trace("put!");

	message m;
	a.getmessage(&m);
	s.trace("message got: %s", encode(&m));
	a.putmessage(&m);
	s.trace("message put: %s", encode(&m));

	sleep(8);

}
catch(const std::exception& e){
	std::cerr << "Exception Caught: " << e.what() << std::endl;
}
