#include "logger.h"

#define FILE_PERMS ( S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH )
#define OPEN_FLAGS ( O_WRONLY | O_APPEND | O_CREAT )

namespace orc {

Logger::Logger(const char *name, Severity level) : level_(level)
{
	char *file = new char[strlen(name)+strlen(".log")+1];
	sprintf(file, "%s.log", name);
	descriptor_ = open(file, OPEN_FLAGS, FILE_PERMS);
	if (descriptor_ < 0) throw std::runtime_error("Logger::Logger!");
	delete[] file;
	indent_ = new char[indentsize];
	*indent_ = '\0';
}

Logger::Logger(int descriptor, Severity level) : level_(level)
{
	descriptor_ = dup(descriptor);
	if (descriptor_ < 0) throw std::runtime_error("Logger::Logger!");
	indent_ = new char[indentsize];
	*indent_ = '\0';
}

Logger::~Logger()
{
	delete[] indent_;
	if (close(descriptor_) < 0) throw std::runtime_error("Logger::~Logger!");
}

void Logger::setindent(const char *indent)
{
	snprintf(indent_, indentsize, "%s", indent);
}

void Logger::setlevel(Severity level)
{
	level_ = level;
}

ssize_t Logger::vprintf(Severity priority, const char *format, va_list arglist)
{
	if (priority > level_) return 0;
	ssize_t n = 0;

	static char indent[0x100];
	n = snprintf(indent, sizeof(indent), "%s: %s", timestamp(), indent_);
	if (n < 0) return -1;

	static char message[0x400];
	n = vsnprintf(message, sizeof(message), format, arglist);
	if (n < 0) return -1;

	static char buffer[0x500];
	n = snprintf(buffer, sizeof(buffer), "%s(%s): %s\n", indent, getseverity(priority), message);
	if (n < 0) return -1;
	return dogwrite(descriptor_, buffer, (size_t)n);
}

char* Logger::timestamp()
{
	static char ts[0x5f];
	time_t t; time(&t);
	strftime(ts, 0x5f, "%Y%m%d%H%M%S", localtime(&t));
	return ts;
}

char* Logger::getseverity(Severity priority)
{
	static char *p = "trace";
	if (priority == Severity::trace) p = "trace";
	if (priority == Severity::error) p = "error";
	if (priority == Severity::stats) p = "stats";
	return p;
}

ssize_t Logger::dogwrite(int filedes, void *buffer, size_t size) {
	ssize_t num;
	while (((num = write(filedes, buffer, size)) == -1) && (errno == EINTR));
	return num;
}


} //namespace orc
