#include "client.h"
#include "timing.h"
#include <iostream>

using namespace orc;

int main()
try {

	Client c("client", 1, "127.0.0.1", 8848);
	c.trace("start running!");

	c.connect();
	c.trace("connected!!!");

	c.put("Hello, World!", 13);
	c.trace("put!");

	char store[14]; store[13] = '\0';
	c.get(store, 14);
	c.trace("get: %s", store);

	sleep(3);

	message m(42090, 12325);
	c.putmessage(&m);
	c.trace("message put: %s", encode(&m));

	message n;
	c.getmessage(&n);
	c.trace("message got: %s", encode(&n));


}
catch(const std::exception& e){
	std::cerr << "Exception Caught: " << e.what() << std::endl;
}
