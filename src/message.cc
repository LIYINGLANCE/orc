#include "message.h"

namespace orc {

message::message(long t, long s)
{
	type = t;
	size = s;
}

message& message::operator=(const message& m)
{
	type = m.type;
	size = m.size;
	return *this;
}

char *encode(message *m)
{
	static char buffer[numdigitsmssg];
	int num = snprintf(buffer, sizeof(buffer), messageformat, m->type, m->size);
	if (num != numdigitschar + numdigitslong ) return NULL;
	return buffer;
}

message decode(char *s)
{
	message m;
	int num = sscanf(s, messageformat, &m.type, &m.size);
	return m;
}

int messagecmp(message s, message t)
{
	return abs(s.type - t.type) + abs(s.size - t.size);
}

int putmessage(int des, message *m)
{
	return write(des, encode(m), numdigitsmssg);
}

int getmessage(int des, message *m)
{
	char buffer[numdigitsmssg];
	int num = read(des, buffer, numdigitsmssg);
	*m = decode(buffer);
	return num;
}


} //namespace orc
