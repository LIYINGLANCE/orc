#include "server.h"

namespace orc {

Server::Server(const char *name, const char *host, short port)
: Applet(name), Socker(host, port)
{
	bind();
}

Server::Server(const char *name, const char *logname, const char *host, short port)
: Applet(name, logname), Socker(host, port)
{
	bind();
}

Server::Server(const char *name, int logdes, const char *host, short port)
: Applet(name, logdes), Socker(host, port)
{
	bind();
}

void Server::bind()
{
	if (::bind(sock_, (struct sockaddr *)(&address_), (socklen_t)(sizeof(address_))) < 0)
		throw std::runtime_error("Server: bind failed!");
	bound_ = true;
}

void Server::listen(int backlog)
{
	if (::listen(sock_, backlog) < 0) throw std::runtime_error("Server: listen failed!");
	backlog_ = backlog;
}

Acceptor Server::accept()
{
	struct sockaddr_in connectoraddress;
	socklen_t sockaddrlength = sizeof(connectoraddress);
	int connectedfd = ::accept(sock_, (struct sockaddr *)(&connectoraddress), &sockaddrlength);
	if (connectedfd < 0) throw std::runtime_error("Server: accept failed!");

	Socker s(connectedfd, connectoraddress, sockaddrlength);
	s.connected();
	return s;
}

} //namespace orc
