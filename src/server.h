#ifndef SERVER_H
#define SERVER_H

#include "socker.h"
#include "applet.h"

namespace orc {

constexpr int listeningQlength = 1024;

class Server : public Applet, public Socker {
public:

	Server(const char *name, const char *host, short port);
	Server(const char *name, const char *logname, const char *host, short port);
	Server(const char *name, int logdes, const char *host, short port);
	~Server(){}

	void bind();
	void listen(int backlog = listeningQlength);
	Acceptor accept();
private:
	int backlog_;
};

} //namespace orc
#endif
